//
//  Blocks.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

typealias ErrorBlock = (_ error: Error?) -> Void
typealias NoArgsVoidBlock = () -> Void
typealias JSONCompletionHandler = (_: [String: Any]?, _: Error?) -> Void

