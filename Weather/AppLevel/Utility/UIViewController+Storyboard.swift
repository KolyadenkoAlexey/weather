//
//  UIViewController+Storyboard.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/28/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

/// Protocol that allows you to create instances of UIViewController from Storyboard using one function
protocol StoryboardInstantiable: class {
    static func initFromStoryboard(withName storyboardName: String?, storyboardIdentifier: String?, inBundle bundle: Bundle?) -> UIViewController?
}

extension UIViewController: StoryboardInstantiable {
    /**
     Instantiate view controller with identifier from storyboard in bundle.
     
     - parameters:
     - storyboardName: File name of the storyboard. Type name of View Controller by default.
     - storyboardIdentifier: Identifier of desired view controller in storyboard. Leave nil for initiateInitialViewController.
     - bundle: Bundle of storyboard. Nil By default
     */
    static func initFromStoryboard(withName storyboardName: String? = nil, storyboardIdentifier: String? = nil, inBundle bundle: Bundle? = nil) -> UIViewController? {
        let storyboardName = storyboardName ?? String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        guard let storyboardIdentifier = storyboardIdentifier else {
            return storyboard.instantiateInitialViewController()
        }
        return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier)
    }
}

