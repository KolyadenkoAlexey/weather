//
//  FetchResultsStateMachine.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/30/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class FetchResultsStateMachine<Result> {
    
    // MARK: - Typealiases
    typealias StateChangedBlock = (_ newState: State, _ fromState: State) -> Void
    typealias ResultsBlock = (_ results: [Result]?) -> Void
    typealias RefreshingBlock = (_ status: RefreshStatus) -> Void
    
    // MARK: - Enums
    enum RefreshStatus {
        case refreshing
        case ended
    }
    
    enum State {
        case empty
        case fetching
        case populated(results: [Result])
        case error(error: Error?)
    }
    
    // MARK: - Data
    var results: [Result] {
        switch currentState {
        case  let .populated(results: results):
            return results
        default:
            return []
        }
    }
    
    // MARK: - init
    init(fillWithResultsBlock: ResultsBlock? = nil, presentErrorBlock: ErrorBlock? = nil, refreshingBlock: RefreshingBlock? = nil, stateChangedBlock: StateChangedBlock? = nil) {
        self.fillWithResultsBlock = fillWithResultsBlock
        self.presentErrorBlock = presentErrorBlock
        self.stateChangedBlock = stateChangedBlock
        self.refreshingBlock = refreshingBlock
    }
    
    // MARK: - Event Blocks
    var fillWithResultsBlock: ResultsBlock?
    var presentErrorBlock: ErrorBlock?
    var stateChangedBlock: StateChangedBlock?
    var refreshingBlock: RefreshingBlock?
    
    // MARK: - Events and state
    var currentState: State = .empty {
        didSet {
            stateChanged(newState: currentState, fromState: oldValue)
        }
    }
    
    func stateChanged(newState: FetchResultsStateMachine.State, fromState: FetchResultsStateMachine.State) {
        switch newState {
        case .empty:
            break
        case .fetching:
            switch fromState { // Using switch here bacause of assosiated values in the enum.
            case .fetching: break
            default:
                refreshingBlock?(.refreshing)
            }
        case let .populated(results):
            filled(with: results)
            break
        case let .error(error):
            presentErrorBlock?(error)
            break
        }
        stateChangedBlock?(newState, fromState)
    }
    
    func filled(with results: [Result]) {
        fillWithResultsBlock?(results)
        refreshingBlock?(.ended)
    }
}
