//
//  MainFlowCoordinator.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/28/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

// MARK: - MainFlowController Data Requirements
protocol MainFlowDataManager {
    var citiesForecastDataSource: CitiesForecastDataProvidable { get }
    var citySearchDataSource: SearchDataProvidable { get }
}

class MainFlowController {
    // MARK: - DataManager:
    var dataManager: MainFlowDataManager!
    
    // MARK: - UIKit related
    var navigationController: UINavigationController?
    
    func configure(with window: UIWindow, dataManager: MainFlowDataManager) {
        self.dataManager = dataManager
        let citiesVC = initializeCitiesVC()
        assert(citiesVC != nil, "Cities View Controller is nil")
        navigationController = UINavigationController()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.viewControllers = [citiesVC!]
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    // MARK: - Controllers inits/configs
    private func initializeCitiesVC() -> UIViewController? {
        guard let citiesVC = CitiesViewController.initFromStoryboard() as? CitiesViewController else {
            return nil
        }
        citiesVC.dataSource = dataManager.citiesForecastDataSource
        citiesVC.citySelectedBlock = { city in
            let cityDetailsVC = self.initializeCityDetailsVC(with: city)
            assert(cityDetailsVC != nil, "City Details VC is nil")
            self.navigationController?.pushViewController(cityDetailsVC!, animated: true)
        }
        citiesVC.searchOpenActionBlock = {
            let searchVC = self.initializeSearchVC()
            assert(searchVC != nil, "City Search VC is nil")
            searchVC?.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(searchVC!, animated: true, completion: nil)
        }
        return citiesVC
    }
    
    private func initializeCityDetailsVC(with city: CityForecastListItemPresentable) -> UIViewController? {
        let controller = CityDetailsViewController.initFromStoryboard() as! CityDetailsViewController
        city.getDetailedForecast { (presentable, error) in // Lets details vc handle error
            guard let presentable = presentable else { return } // Error handling should be here :)
            controller.city = presentable
        }
        return controller
    }
    
    private func initializeSearchVC() -> UIViewController? {
        let searchViewController = CitySearchViewController.initFromStoryboard() as? CitySearchViewController
        searchViewController?.dataSource = dataManager.citySearchDataSource
        searchViewController?.cancelClickedBlock = {
            searchViewController?.dismiss(animated: true, completion: nil)
        }
        searchViewController?.searchResultSelectedBlock = { result, error in
            guard let error = error else {
                searchViewController?.searchController?.isActive = false
                searchViewController?.dismiss(animated: true, completion: nil)
                return
            }
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            searchViewController?.present(alert, animated: true, completion: nil)
        }
        return searchViewController
    }
}
