//
//  Dummy.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import MapKit

// MARK: - ForecastDataSource API Requirement Protocols
typealias CitySearchCompletionHandler = (_: [CityForecast]?, _: Error?) -> Void
typealias CityForecastCompletionHandler = (_: CityForecast?, _: Error?) -> Void

protocol ForecastAPI {
    func forecastCurrentWeather(forModel model: CityForecast, completionHandler: CityForecastCompletionHandler?)
}

protocol CitySearchAPI {
    func search(term: String, completionHander: CitySearchCompletionHandler?)
}



// MARK: - DataSource, implementation of VC requirements
class ForecastDataSource: CitiesForecastDataProvidable, SearchDataProvidable {
    
    // MARK: - Properties
    var dataUpdatedBlock: CityForecastArrayUpdatedBlock?
    var myForecasts = Set<CityForecastViewModel>()
    
    // MARK: - APIs
    struct RemoteAPI {
        var forecastAPI: ForecastAPI!
        var citySearchAPI: CitySearchAPI!
    }
    
    var api: RemoteAPI

    // MARK: - Init
    init(api: RemoteAPI) {
        self.api = api
    }
    
    // MARK: - Public, protocol level
    func getMyForecasts(with completionHandler: CitiesForecastFetchResult?) {
        updateForecast(forCities: Array(myForecasts)) { (forecasts) in
            completionHandler?(forecasts, nil)
            self.dataUpdatedBlock?(forecasts)
        }
    }
    
    func performSearch(with term: String, completionHandler: SearchResultsBlock?) {
        api.citySearchAPI.search(term: term) { (response, error) in
            let viewModels = response?.map({ (item) -> CityForecastViewModel in
                let viewModel = CityForecastViewModel(with: item)
                viewModel.dataSource = self
                return viewModel
            })
            
            completionHandler?(viewModels, error)
        }
    }

    // MARK: - ViewModelRelated
    func updateForecast(forCity city: CityForecastViewModel, completionHandler: CityForecastUpdatedBlock?) {
        api.forecastAPI.forecastCurrentWeather(forModel: city.model, completionHandler: { (response, error) in
            completionHandler?(city)
        })
    }
    
    func updateForecast(forCities cities: [CityForecastViewModel], completionHandler: CityForecastArrayUpdatedBlock?) {
        let group = DispatchGroup()
        for city in cities {
            group.enter()
            updateForecast(forCity: city, completionHandler: { (model) in
                group.leave()
            })
        }
        
        group.notify(queue: DispatchQueue.main) {
            completionHandler?(cities)
        }
    }
    
    func addCity(city: CityForecastViewModel, with completionHandler: ErrorBlock?) {
        updateForecast(forCity: city) { (city) in
            completionHandler?(nil)
            self.dataUpdatedBlock?(Array(self.myForecasts))
        }
        myForecasts.insert(city)
    }
    
    func removeCity(city: CityForecastViewModel, with completionHandler: ErrorBlock?) {
        myForecasts.remove(city)
        completionHandler?(nil)
        dataUpdatedBlock?(Array(self.myForecasts))
    }
    

}
