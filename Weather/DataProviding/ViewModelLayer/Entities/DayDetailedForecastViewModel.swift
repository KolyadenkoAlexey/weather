//
//  DayDetailedForecastViewModel.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/3/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class DayDetailedForecastViewModel: DayForecastViewModel, DayDetailedForecastItemPresentable {
    var realTemperature: String? {
        guard let temperature = model.temperature else { return nil }
        return maxMinString(fromValue: temperature)
    }
    
    var apparentTemperature: String? {
        guard let temperature = model.apparentTemperature else { return nil }
        return maxMinString(fromValue: temperature)
    }
    
    func maxMinString(fromValue value: WeatherParameterRange<Double>) -> String? {
        guard let max = value.max, let min = value.min else { return nil }
        guard let maxString = CityForecastViewModel.celsiumTemperatureString(from: max) else { return nil }
        guard let minString = CityForecastViewModel.celsiumTemperatureString(from: min) else { return nil }
        return "from \(minString) to \(maxString)"
    }
    
    var summary: String? {
        return model.summary
    }
    
    var intensityOfRain: String? {
        guard let intensity = model.precipIntensity?.present else { return nil }
        return DayDetailedForecastViewModel.getPercentsString(fromDouble: intensity)
    }
    var chanceOfRain: String? {
        guard let chance = model.chanceOfRain else { return nil }
        return DayDetailedForecastViewModel.getPercentsString(fromDouble: chance)
    }
    
    class func getPercentsString(fromDouble doubleValue: Double) -> String {
        let percents = doubleValue
        return "\(Int(percents))%"
    }
    
    var humidity: String? {
        guard let humid = model.humidity else { return nil }
        return DayDetailedForecastViewModel.getPercentsString(fromDouble: humid)
    }
    
    var windSpeed: String? {
        guard let speed = model.windSpeed else { return nil }
        return "\(speed) KPH"
    }
    
    var uvIndex: String? {
        guard let index = model.uvIndex else { return nil }
        return "\(Int(index))"
    }
}
