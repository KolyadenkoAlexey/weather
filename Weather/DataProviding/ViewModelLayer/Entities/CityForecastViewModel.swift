//
//  DarkSkyMappedViewModels.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/1/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

// MARK: - ViewModels, Implementation of ViewControllers requirements with model mapping
class CityForecastViewModel: CityForecastListItemPresentable, CityForecastDetailsPresentable {
    var model: CityForecast
    
    weak var dataSource: ForecastDataSource!
    
    init(with model: CityForecast) {
        self.model = model
    }
    
    var days: [DayDetailedForecastItemPresentable] {
        return (model.daily?.points?.map({ return DayDetailedForecastViewModel(with: $0) }))!
    }
    
    func getDetailedForecast(withCompletionHandler completionHandler: DetailedForecastBlock?) {
        completionHandler?(self, nil) // In case we need separate request for details.
    }

    
    var placeName: String? { return model.name }
    var currentTemperature: String? {
        guard let currentTemperatureValue = model.currently?.temperature?.present else { return nil }
        return CityForecastViewModel.celsiumTemperatureString(from: currentTemperatureValue)
    }
    
    class func celsiumTemperatureString(from temperature: Temperature) -> String? {
        let roundedTemperature = Int(temperature.rounded())
        return "\(roundedTemperature) °C"
    }
    
    var currentIconEmoji: String? {
        guard let icon = model.currently?.icon else { return nil }
        return CityForecastViewModel.emoji(forIconName: icon)
    }
    
    class func emoji(forIconName iconName: String) -> String? {
        switch iconName {
        case "rain":
            return "🌧️"
        case "clear-day":
            return "☀️"
        case "clear-night":
            return "🌃"
        case "snow":
            return "❄️"
        case "sleet":
            return "⛸️"
        case "fog":
            return "🌫️"
        case "cloudy":
            return "🌥️"
        case "partly-cloudy-day":
            return "🌤️"
        case "partly-cloudy-night":
            return "☁️"
        case "hail":
            return "🍨"
        case "thunderstorm":
            return "🌩️"
        case "tornado":
            return "🌪️"
        case "hurricane":
            return "🌀"
        default:
            return nil
        }
    }
    
    func deleteFromList(withCompletionHandler completionHandler: ErrorBlock?) {
        dataSource.removeCity(city: self, with: completionHandler)
    }
}

extension CityForecastViewModel: Hashable { // Comparing by name. I know it's better to have ids though...
    public var hashValue: Int { return model.name?.hashValue ?? 0 }
    public static func ==(lhs: CityForecastViewModel, rhs: CityForecastViewModel) -> Bool { return lhs.model.name == rhs.model.name }
}

extension CityForecastViewModel: SearchItemPresentable {
    var name: String? { return model.name }
    
    func select(withCompletionHandler handler: ErrorBlock?) {
        dataSource?.addCity(city: self, with: handler)
    }
}



