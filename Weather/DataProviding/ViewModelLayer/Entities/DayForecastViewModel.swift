//
//  DayForecastViewModel.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/3/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class DayForecastViewModel: DayForecastItemPresentable {
    var model: WeatherDataPoint
    
    var details: DayDetailedForecastItemPresentable? {
        return nil
    }
    
    init(with model: WeatherDataPoint) {
        self.model = model
    }
    
    var dayTitle: String? {
        guard let timestamp = model.timestamp else { return nil }
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        return dayInWeek
    }
    
    var emoji: String? {
        guard let iconName = model.icon else { return nil }
        return CityForecastViewModel.emoji(forIconName: iconName)
    }
    
    var temperature: String? {
        guard let temperature = model.temperature?.max else { return nil }
        return CityForecastViewModel.celsiumTemperatureString(from: temperature)
    }
}
