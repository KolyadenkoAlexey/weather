//
//  DataSourceManager.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class DataSourceManager: MainFlowDataManager {
    var dataSource: ForecastDataSource
    init() {
        self.dataSource = ForecastDataSource(api: ForecastDataSource.RemoteAPI(forecastAPI: DarkSkyForecastAPI(), citySearchAPI: WUCitySearchAPI()))
    }
    
    var citiesForecastDataSource: CitiesForecastDataProvidable { return dataSource } // In case of mocking different DataProvidables
    var citySearchDataSource: SearchDataProvidable { return dataSource }
}
