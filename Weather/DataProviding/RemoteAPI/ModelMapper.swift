//
//  ModelMapper.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 7/31/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import CoreLocation

extension CityForecast {
    convenience init(withDictionary dictionary: [String: Any]) {
        self.init(withName: dictionary["name"] as? String, coordinate: nil)
        if let latStr = dictionary["lat"] as? String, let lonStr = dictionary["lon"] as? String {
            if let latitude = Double(latStr), let longitude = Double(lonStr) {
                coordinate = CLLocationCoordinate2D(latitude: latitude, longitude:longitude)
            }
        }
    }
    
    func updateForecast(withDictionary dictionary: [String: Any]) {
        if let currentlyDict = dictionary["currently"] as? [String: Any] {
            self.currently = WeatherDataPoint(withDictionary: currentlyDict)
        }
        
        if let hourlyDict = dictionary["hourly"] as? [String: Any] {
            self.hourly = WeatherDataBlock(withDictionary: hourlyDict)
        }
        
        if let dailyDict = dictionary["daily"] as? [String: Any] {
            self.daily = WeatherDataBlock(withDictionary: dailyDict)
        }
    }
}

extension WeatherDataPoint {
    convenience init(withDictionary dictionary: [String: Any]) {
        self.init()
        timestamp = dictionary["time"] as? Double
        icon = dictionary["icon"] as? String
        summary = dictionary["summary"] as? String
        
        apparentTemperature = WeatherDataPoint.mapParameter(withName: "apparentTemperature", toRangeFromDictionary: dictionary)
        temperature = WeatherDataPoint.mapParameter(withName: "temperature", toRangeFromDictionary: dictionary)
        precipIntensity = WeatherDataPoint.mapParameter(withName: "precipIntensity", toRangeFromDictionary: dictionary)
        
        chanceOfRain = dictionary["precipProbability"] as? Double
        humidity = dictionary["humidity"] as? Double
        windSpeed = dictionary["windSpeed"] as? Double
        uvIndex = dictionary["uvIndex"] as? Double
    }
    
    static func mapParameter<Parameter>(withName name: String, toRangeFromDictionary dictionary: [String: Any]) -> WeatherParameterRange<Parameter> {
        return WeatherParameterRange<Parameter>.init(min: dictionary["\(name)Min"] as? Parameter, present: dictionary["\(name)"] as? Parameter, max: dictionary["\(name)Max"] as? Parameter)
    }
}

extension WeatherDataBlock {
    convenience init(withDictionary dictionary: [String: Any]) {
        self.init()
        summary = dictionary["summary"] as? String
        icon = dictionary["icon"] as? String
        if let dataArray = dictionary["data"] as? [[String: Any]] {
            points = dataArray.map { WeatherDataPoint(withDictionary: $0) }
        }
    }
}
