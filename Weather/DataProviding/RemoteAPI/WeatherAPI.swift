//
//  WeatherAPI.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/30/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import Alamofire

class ForecastAPI {
    let accessCode = "8f2856f1241e19934040797e203336fb"
    
    func forecastCurrentWeather(forModel model: CityForecast, completionHandler: CityForecastCompletionHandler?) {
        guard let coordinate = model.coordinate else {
            completionHandler?(nil, nil)
            return
        }
        let url = "https://api.darksky.net/forecast/\(accessCode)/\(coordinate.latitude),\(coordinate.longitude)?units=si"
        Alamofire.request(url).responseJSON { response in
            if let forecastDict = response.result.value as? [String: Any] {
                model.updateForecast(withDictionary: forecastDict)
            }
            completionHandler?(model, response.error)
        }
    }
}

typealias CitySearchCompletionHandler = (_: [CityForecast]?, _: Error?) -> Void
typealias CityForecastCompletionHandler = (_: CityForecast?, _: Error?) -> Void

class CitySearchAPI { // Using two different APIs because first one has autocomplete and second one has 5-days forecast
    func search(term: String, completionHander: CitySearchCompletionHandler?) {
        Alamofire.request("http://autocomplete.wunderground.com/aq?query=\(term)").responseJSON { (response) in
            let responseDict = response.result.value as? [String: Any]
            let responseArray = responseDict?["RESULTS"] as? [[String: Any]]
            let items = responseArray?.flatMap({ (dict) -> CityForecast? in
                return CityForecast(withDictionary: dict)
            })
            completionHander?(items, response.error)
        }
    }
}
