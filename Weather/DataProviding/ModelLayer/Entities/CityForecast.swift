//
//  CityForecast.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/30/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import CoreLocation

typealias Temperature = Double

class CityForecast {
    var coordinate: CLLocationCoordinate2D?
    var name: String?
    
    var currently: WeatherDataPoint?
    var hourly: WeatherDataBlock?
    var daily: WeatherDataBlock?
    
    var iconName: String?
    
    init(withName name: String?, coordinate: CLLocationCoordinate2D?) {
        self.name = name
        self.coordinate = coordinate
    }
}




