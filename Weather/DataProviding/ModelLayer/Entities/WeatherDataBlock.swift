//
//  WeatherDataBlock.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/3/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class WeatherDataBlock {
    var points: [WeatherDataPoint]?
    var icon: String?
    var summary: String?
}
