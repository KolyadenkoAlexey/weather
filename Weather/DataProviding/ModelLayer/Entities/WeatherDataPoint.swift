//
//  WeatherDataPoint.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/3/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class WeatherDataPoint {
    var timestamp: TimeInterval?
    var icon: String?
    var summary: String?
    var apparentTemperature: WeatherParameterRange<Temperature>?
    var temperature: WeatherParameterRange<Temperature>?
    var precipIntensity: WeatherParameterRange<Double>?
    var chanceOfRain: Double?
    var humidity: Double?
    var windSpeed: Double?
    var uvIndex: Double?
}
