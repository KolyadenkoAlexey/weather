//
//  WeatherParameterRange.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/3/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

struct WeatherParameterRange<Parameter> {
    var min: Parameter?
    var present: Parameter?
    var max: Parameter?
}
