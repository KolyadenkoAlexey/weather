//
//  CityDetailsViewControllerCells.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/1/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            setSelected(selected: isSelected, animated: true)
        }
    }
    
    func setSelected(selected isSelected: Bool, animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.contentView.alpha = isSelected ? 1 : 0.6
            })
        }   else {
            self.contentView.alpha = isSelected ? 1 : 0.6
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setSelected(selected: false, animated: false)
        iconLabel.text = nil
        titleLabel.text = nil
        temperatureLabel.text = nil
    }
}
