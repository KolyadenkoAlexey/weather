//
//  CityDetailsViewController.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

// MARK: - Cells
let DayCollectionViewCellIdentifier = "DayCollectionViewCell"
let DayDetailsParameterViewCellIdentifier = "DayDetailsParameterViewCell"
let GraphCollectionViewCellIdentifier = "GraphCollectionViewCellIdentifier"

// MARK: - Controller, Views
class CityDetailsViewController: UIViewController {
    
    // MARK: - Data Props
    var city: CityForecastDetailsPresentable? {
        didSet {
            refreshData()
        }
    }
    
    var selectedDayIndex: Int = 0 {
        didSet {
            navigationItem.title = selectedDay?.dayTitle
            dayDetailsTableView?.reloadSections(IndexSet.init(integer: 0), with: .automatic)
        }
    }
    
    var selectedDay: DayDetailedForecastItemPresentable? {
        return city?.days[selectedDayIndex]
    }
    
    // MARK: - VC Props
    @IBOutlet weak var daysCollectionView: UICollectionView? {
        didSet {
            daysCollectionView?.delegate = self
            daysCollectionView?.dataSource = self
        }
    }
    
    @IBOutlet weak var dayDetailsTableView: UITableView? {
        didSet {
            dayDetailsTableView?.delegate = self
            dayDetailsTableView?.dataSource = self
            dayDetailsTableView?.tableFooterView = UIView()
        }
    }

    
    // MARK: - Utility
    func refreshData() {
        daysCollectionView?.reloadSections(IndexSet.init(integer: 0))
    }

    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()
    }
}

extension CityDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return dayCell(forIndexPath: indexPath)!
    }
    
    func dayCell(forIndexPath indexPath: IndexPath) -> DayCollectionViewCell? {
        guard let daysCollectionView = daysCollectionView else { return nil }
        let cell = daysCollectionView.dequeueReusableCell(withReuseIdentifier: DayCollectionViewCellIdentifier, for: indexPath) as! DayCollectionViewCell
        if let day = city?.days[indexPath.row] {
            cell.iconLabel.text = day.emoji
            cell.titleLabel.text = day.dayTitle
            cell.temperatureLabel.text = day.temperature
            cell.setSelected(selected: selectedDayIndex == indexPath.row, animated: false)
        }
        return cell
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return city?.days.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDayIndex = indexPath.row
        refreshData()
    }
}

extension CityDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedDay != nil ? 8 : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DayDetailsParameterViewCellIdentifier, for: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = selectedDay?.summary
            cell.detailTextLabel?.text = nil
        case 1:
            cell.textLabel?.text = "Temperature"
            cell.detailTextLabel?.text = selectedDay?.realTemperature
        case 2:
            cell.textLabel?.text = "Apparently"
            cell.detailTextLabel?.text = selectedDay?.apparentTemperature
        case 3:
            cell.textLabel?.text = "Intensity of precipitation"
            cell.detailTextLabel?.text = selectedDay?.intensityOfRain
        case 4:
            cell.textLabel?.text = "Chance of rain"
            cell.detailTextLabel?.text = selectedDay?.chanceOfRain
        case 5:
            cell.textLabel?.text = "Humidity"
            cell.detailTextLabel?.text = selectedDay?.humidity
        case 6:
            cell.textLabel?.text = "Wind speed"
            cell.detailTextLabel?.text = selectedDay?.windSpeed
        case 7:
            cell.textLabel?.text = "UV Index"
            cell.detailTextLabel?.text = selectedDay?.uvIndex
        default:
            break
        }
        return cell
    }
}
