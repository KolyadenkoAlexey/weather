//
//  CityDetailsViewControllerRelatedTypes.swift
//  Weather
//
//  Created by Oleksii Koliadenko on 8/1/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

protocol CityForecastDetailsPresentable {
    var days: [DayDetailedForecastItemPresentable] { get }
}

protocol DayForecastItemPresentable {
    var dayTitle: String? { get }
    var emoji: String? { get }
    var temperature: String? { get }
}

protocol DayDetailedForecastItemPresentable: DayForecastItemPresentable {
    var realTemperature: String? { get }
    var apparentTemperature: String? { get }
    var summary: String? { get }
    var intensityOfRain: String? { get }
    var chanceOfRain: String? { get }
    var humidity: String? { get }
    var windSpeed: String? { get }
    var uvIndex: String? { get }
}
