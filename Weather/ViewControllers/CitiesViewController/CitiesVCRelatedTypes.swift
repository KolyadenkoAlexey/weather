//
//  CitiesVCRelatedTypes.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit
import CoreLocation

// MARK: - CityForecastListItemPresentable Requirements
typealias DetailedForecastBlock = (_: CityForecastDetailsPresentable?, _: Error?) -> Void
protocol CityForecastListItemPresentable {
    var placeName: String? { get }
    var currentTemperature: String? { get }
    var currentIconEmoji: String? { get }
    
    func deleteFromList(withCompletionHandler completionHandler: ErrorBlock?)
    func getDetailedForecast(withCompletionHandler completionHandler: DetailedForecastBlock?)
}

// MARK: - DataSource Requirements
typealias CitiesForecastFetchResult = (_ result: [CityForecastListItemPresentable]?, _ error: Error?) -> Void
typealias CityForecastUpdatedBlock = (_: CityForecastListItemPresentable) -> Void
typealias CityForecastArrayUpdatedBlock = (_: [CityForecastListItemPresentable]) -> Void

protocol CitiesForecastDataProvidable {
    func getMyForecasts(with completionHandler: CitiesForecastFetchResult?)    
    var dataUpdatedBlock: CityForecastArrayUpdatedBlock? { get set }
}
