//
//  CitiesViewController.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/28/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

typealias CitySelectedBlock = (_ city: CityForecastListItemPresentable) -> Void

private let CityTableViewCellIdentifier = "CityTableViewCell"

class CitiesViewController: UIViewController {
    
    // MARK: - Flow
    var citySelectedBlock: CitySelectedBlock?
    var searchOpenActionBlock: (() -> Void)?
    
    // MARK: - State
    var stateMachine: FetchResultsStateMachine<CityForecastListItemPresentable>!
    
    func configureStateMachine(withTableView tableView: UITableView) {
        self.stateMachine = FetchResultsStateMachine<CityForecastListItemPresentable>.init(fillWithResultsBlock: { (results) in
            tableView.reloadSections(IndexSet.init(integer: 0), with: .automatic)
        }, presentErrorBlock: { (error) in
            self.presentError(error: error)
        }, refreshingBlock: { (status) in
            switch status {
            case .refreshing:
                tableView.refreshControl?.beginRefreshing()
            case .ended:
                tableView.refreshControl?.endRefreshing()
            }
        }, stateChangedBlock: nil)
    }
    
    // MARK: - Data props
    var dataSource: CitiesForecastDataProvidable? {
        didSet {
            dataSource?.dataUpdatedBlock = { (updated) in
                self.stateMachine.currentState = .populated(results: updated)
            }
        }
    }
    
    var places: [CityForecastListItemPresentable] {
        return stateMachine.results
    }
    
    // MARK: Data/Presentation related functions
    func populateCities() {
        self.stateMachine.currentState = .fetching
        dataSource?.getMyForecasts(with: { (results, error) in
            guard let results = results else {
                self.stateMachine.currentState = .error(error: error)
                return
            }
            self.stateMachine.currentState = .populated(results: results)
        })
    }
    
    func presentError(error: Error?) {
        
    }
    
    // MARK: - VC Props
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            guard let tableView = tableView else { return }
            tableView.delegate = self
            tableView.dataSource = self
            
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(CitiesViewController.refreshRequested(sender:)), for: UIControlEvents.valueChanged)
            tableView.refreshControl = refreshControl
            
            configureStateMachine(withTableView: tableView)
        }
    }

    // MARK: - Actions
    func refreshRequested(sender: Any) {
        populateCities()
    }
    
    func openSearch() {
        searchOpenActionBlock?()
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(CitiesViewController.openSearch))
        navigationItem.rightBarButtonItem = addBarButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        populateCities()
    }
}

extension CitiesViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCellIdentifier, for: indexPath)
        let viewModel = places[indexPath.row]
        cell.textLabel?.text = viewModel.placeName
        if let icon = viewModel.currentIconEmoji, let temperature = viewModel.currentTemperature {
            cell.detailTextLabel?.text = "\(icon) \(temperature)"
        }
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            places[indexPath.row].deleteFromList(withCompletionHandler: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = places[indexPath.row]
        citySelectedBlock?(viewModel)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
