//
//  CitySearchViewController.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import UIKit

private let SearchResultCellIdentifier = "SearchResultCell"


class CitySearchViewController: UIViewController {
    
    // MARK: - Typealiases
    typealias SearchResultSelectedBlock = (_: SearchItemPresentable, _: Error?) -> Void
    
    // MARK: - Flow
    var cancelClickedBlock: NoArgsVoidBlock?
    var searchResultSelectedBlock: SearchResultSelectedBlock?
    
    // MARK: - State
    var stateMachine: FetchResultsStateMachine<SearchItemPresentable>!
    
    func configureStateMachine(withTableView tableView: UITableView) {
        self.stateMachine = FetchResultsStateMachine<SearchItemPresentable>.init(stateChangedBlock: { (newState, oldState) in
            tableView.reloadData()
        })
    }

    // MARK: - Data
    var dataSource: SearchDataProvidable?
    
    var results: [SearchItemPresentable] {
        return stateMachine.results
    }
    
    // MARK: - VC Props
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            guard let tableView = tableView else {
                return
            }
            tableView.delegate = self
            tableView.dataSource = self
            
            configureStateMachine(withTableView: tableView)
        }
    }
    
    var searchController: UISearchController?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configireSearchController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController?.isActive = true
    }
    
    // MARK: - Config
    func configireSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.showsCancelButton = true
        searchController.delegate = self
        definesPresentationContext = true
        tableView?.tableHeaderView = searchController.searchBar
        self.searchController = searchController
        self.searchController?.searchBar.delegate = self
    }
}

extension CitySearchViewController: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else {
            return
        }
        guard text.characters.count > 2 else {
            stateMachine.currentState = .empty
            return
        }
        dataSource?.performSearch(with: text, completionHandler: { (results, error) in
            guard let results = results else {
                self.stateMachine.currentState = .empty
                return
            }
            self.stateMachine.currentState = .populated(results: results)
        })
    }
}

extension CitySearchViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCellIdentifier, for: indexPath)
        let viewModel = results[indexPath.row]
        cell.textLabel?.text = viewModel.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = results[indexPath.row]
        viewModel.select { (error) in
            self.searchResultSelectedBlock?(viewModel, error)
        }
    }
}


extension CitySearchViewController: UISearchControllerDelegate, UISearchBarDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelClickedBlock?()
    }
}
