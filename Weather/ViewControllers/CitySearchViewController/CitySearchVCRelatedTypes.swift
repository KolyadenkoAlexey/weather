//
//  CitySearchVCRelatedTypes.swift
//  Weather
//
//  Created by Alexey Kolyadenko on 7/29/17.
//  Copyright © 2017 Alexey Kolyadenko. All rights reserved.
//

import Foundation

// MARK: - ViewModel/Model
typealias SearchResultsBlock = (_ results: [SearchItemPresentable]?, _ error: Error?) -> Void
protocol SearchDataProvidable {
    func performSearch(with term: String, completionHandler: SearchResultsBlock?)
}


typealias SearchViewModelSelectionBlock = (_: SearchItemPresentable) -> Void
protocol SearchItemPresentable {
    var name: String? { get }
    func select(withCompletionHandler handler: ErrorBlock?)
}
